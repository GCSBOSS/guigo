import { State } from './state';

State.diagErrors = State.diagErrors ?? [];

export function captureDOM(){
    let html = document.documentElement.outerHTML;
    html = html.replace(/<script/g, '<noscript');
    html = html.replace(/<\/script/g, '<\/noscript');

    for(const ss of document.styleSheets){
        if(ss.href?.substring(0, 4) == 'data')
            continue;
        html = html.replaceAll(ss.href, 'data:text/css;base64,' + btoa(ss.cssRules));
    }

    const imgs = document.querySelectorAll('img');
    for(const i of imgs){
        if(i.currentSrc?.substring(0, 4) == 'data')
            continue;

        const canvas = document.createElement('canvas');
        const context = canvas.getContext('2d');
        canvas.width = i.width;
        canvas.height = i.height;
        context.drawImage(i, 0, 0, canvas.width, canvas.height);
        const uri = context.getImageData(0, 0, canvas.width, canvas.height);
        html = html.replaceAll(i.currentSrc, uri);
    }

    // navigator.clipboard.writeText(html);
    return html;
}

async function getObjectMD5(obj) {
    const jsonString = JSON.stringify(obj);
    const encoder = new TextEncoder();
    const data = encoder.encode(jsonString);
  
    const hashBuffer = await crypto.subtle.digest('SHA-1', data);
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const md5Hash = hashArray.map(byte => byte.toString(16).padStart(2, '0')).join('');
  
    return md5Hash;
}

export async function reportError(ev, err, cb){

    try{
        const bug = {
            ...ev,
            at: new Date(),
            dom: captureDOM(),
            err: {
                type: typeof err,
                klass: err?.constructor?.name,
                message: err.message,
                name: err.name,
                stack: err.stack,
                representation: err.toString()
            },
            klass: ev?.constructor?.name
        };

        bug.hash = await getObjectMD5(bug.err);
        
        State.diagErrors.push(bug);
        cb(bug)?.catch(() => null);
    }
    catch(err){
    }
}

