import m from 'mithril';
import './text-field.css';
import { getNameFromString, filterAttrs } from './util';

export function TextField (){

    let name, data = {}, errors = {}, submit;
    let debounceTo;

    return {

        oncreate(vnode){

            vnode.dom.control.addEventListener('input', function(e){
                delete errors[name];
                data[name] = vnode.dom.control.value;

                if(vnode.attrs.dateTime && data[name])
                    data[name] = new Date(data[name]);

                if(vnode.attrs.oninput && vnode.attrs.debounce){
                    clearTimeout(debounceTo);
                    debounceTo = setTimeout(vnode.attrs.oninput.bind(this, e), vnode.attrs.debounce);
                }
                else
                    vnode.attrs.oninput?.(e);

                m.redraw();
            });

            vnode.dom.control.addEventListener('keyup', function(ev){
                if(ev.key == 'Enter')
                    typeof submit == 'function' && data[name] && submit.call(this, ev);
                m.redraw();
            });
        },

        view(vnode){
            name = vnode.attrs.name || getNameFromString(vnode.attrs.label || '');
            data = vnode.attrs.bind || {};
            errors = vnode.attrs.bindError || {};

            const attrs = filterAttrs(vnode.attrs, 'name', 'type', 'maxlength', 'tabindex',
                'disabled', 'placeholder', 'onkeyup', 'autocomplete', 'onclick');
            attrs.value = vnode.attrs.value ?? data[name];

            submit = vnode.attrs.submit

            let tag = 'input';

            if(vnode.attrs.wrap)
                tag = 'textarea';

            if(vnode.attrs.dateTime){
                attrs.type = 'datetime-local';
                if(data[name] instanceof Date)
                    attrs.value = data[name].toLocaleString('sv', { timeZoneName: undefined }).replace(' ', 'T');
            }

            if(vnode.attrs.secret)
                attrs.type = 'password';

            return m('label.text-field' + (vnode.attrs.class ?? ''), { 'data-error': errors[name] }, [
                m('span', vnode.attrs.label),
                vnode.attrs.icon &&
                    m('span', { 'data-icon': vnode.attrs.icon }),
                m(tag, attrs)
            ])
        }

    };

}

TextField.formControl = true;
