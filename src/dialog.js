import m from 'mithril';
import './dialog.css';
import { FreeBox } from './free-box';

export const dialogState = {
    children: [],
    open: false
};

export function dialog(children){
    if(typeof children == 'function')
        children = children(() => dialog(false));
    dialogState.children = children;
    dialogState.open = Boolean(children);    
    m.redraw();
}

// export function Dialog (){

//     let onclose;

//     // function onRootClick(ev){
//     //     const rect = ev.target.getBoundingClientRect();
//     //     if(ev.clientX > rect.right || ev.clientX < rect.left ||
//     //     ev.clientY > rect.bottom || ev.clientY < rect.top)
//     //         onclose?.(ev);
//     // }

//     return {

//         view(vnode){
//             const { onclose: outOnClose } = vnode.attrs;
//             onclose = () => {
//                 dialog(false);
//                 outOnClose?.()
//             };
//             // return m('dialog', { ...attrs, onclick: onRootClick }, vnode.children);
//             return m(Panel, {
//                 as: 'dialog',
//                 onblur: onclose,
//             }, vnode.children)
//         }

//     };

// }

export function Dialog(){

    return {

        view(vnode){
            const { ...rest } = vnode.attrs;

            return m(FreeBox, {
                ...rest,
                as: 'dialog',
                bind: dialogState,
                name: 'open',
                // uniqueKey: 'guigoDialog',
                routing: true, 
                modal: true
            }, vnode.children)
        }
    }
}



