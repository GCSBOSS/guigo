import m from 'mithril';
import { clickRipple } from './util';

export function Button(){

    let action, noBubble;

    async function onclick(ev){
        if(noBubble)
            ev.stopPropagation();
        await clickRipple(this, ev);
        this.disabled = true;
        this.setAttribute('data-clicked', '');
        setTimeout(() => {
            this.removeAttribute('data-clicked');
            m.redraw();
        }, 1000);


        try{
            if(typeof action == 'function')
                await action.call(this, ev);
        }
        catch(err){
            throw err;
        }
        finally{
            this.disabled = false;
        }
    }

    return {

        view(vnode){

            const { noBubble: nb, class: cls, text, action: act, icon, ...attrs } = vnode.attrs;
            action = act;
            noBubble = nb;

            if(icon)
                attrs[text ? 'data-icon-pre' : 'data-icon'] = icon;

            attrs.type = attrs.type ?? 'button';

            return m('button' + (cls ?? ''), { ...attrs, onclick }, text);
        }

    };

}

const a = m(Button, {  

})