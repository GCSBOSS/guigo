
import m from 'mithril';
import { animate } from './util';
// import { Button } from './button';
import './snack-bar.css';

export const snackState = { snackQueue: [] }

export async function pullSnack(){
    const snackOpts = snackState.snackQueue[0];

    // TODO probably change delay to delay pushing to 
    //      array instead of pulling. So to not stall 
    //      the queue
    if(snackOpts.delay)
        await new Promise(done => setTimeout(done, snackOpts.delay * 1e3));

    snackState.snackBar = snackOpts;
    m.redraw();

    await new Promise(done => setTimeout(done, 4e3));
    snackState.snackBar = false;
    m.redraw();

    await new Promise(done => setTimeout(done, 1e3));
    snackState.snackQueue.shift();

    if(snackState.snackQueue.length > 0)
        pullSnack();
}

export function snackBar(text, opts = {}){
    snackState.snackQueue.push({ ...opts, text });
    if(snackState.snackQueue.length == 1)
        pullSnack();
}

export function SnackBar(){

    return {

        oncreate(vnode){
            animate(vnode.dom,
                vnode.dom.parentNode.childNodes.length == 1
                    ? [ { marginBottom: '-50px' }, { marginBottom: '0' } ]
                    : [ { opacity: 0 }, { opacity: 1 } ],
                { duration: 300 });
        },

        view(vnode){
            // TODO pass attributes in
            return m('aside.snack-bar.' + vnode.attrs.class, vnode.attrs.text);
        },

        async onbeforeremove(vnode){
            await animate(vnode.dom,
                vnode.dom.parentNode.childNodes.length == 1
                    ? [ { marginBottom: '0' }, { marginBottom: '-50px' } ]
                    : [ { opacity: 1 }, { opacity: 0 } ],
                { duration: 200 });
        }
    };
}
