
import m from 'mithril';
import { Button } from './button';
import { getNameFromString } from './util';
import './chips.css';

export function Chip(){

    return {

        view(vnode){
            const text = vnode.attrs.text || 'Chip';

            return m('article.chip', [
                text,

                typeof vnode.attrs.onClose == 'function' &&
                    m(Button, { icon: 'close', action: vnode.attrs.onClose })
            ]);
        }

    };
}

export function ChipList(){

    return {

        view(vnode){
            return m('section.chips', vnode.attrs.items.map(d => m(Chip, d)));
        }

    }

}

export function ChipsField(){

    let name, data = {}, errors = {}, delimiters, onadd, onremove;
    const values = new Set();

    function removeChip(text = false){
        text = text.trim();
        values.delete(text);
        data[name] = Array.from(values.values());
        onremove?.(text);
    }

    function addChip(text){
        text = text.trim();
        values.add(text);
        data[name] = Array.from(values.values());
        onadd?.(text);
    }

    return {

        view(vnode){


            const { onremove: _onremove, onadd: _onadd, delimiters: dels, bind,
                bindError, name: nm,
                placeholder, label, class: cls, ...attrs } = vnode.attrs;

            onadd = _onadd;
            onremove = _onremove;
            delimiters = new RegExp('[' + (dels ?? ',;') + ']');
            name = nm ?? getNameFromString(label ?? '');
            data = bind ?? {};
            errors = bindError ?? {};

            attrs['data-error'] = errors[name];

            return m('label.chips' + (cls ?? ''), attrs, [
                m('span', label),

                // (vnode.attrs.helper || vnode.attrs.error)
                //     && m('span.helper', vnode.attrs.error || vnode.attrs.helper),

                m('div',
                    m('input', { type: 'text', placeholder }),
                    data[name]?.map(i => m(Chip, { text: i, onClose: () => removeChip(i) }))
                ),

                // m(
                //     'select[hidden][multiple]',
                //     { name, disabled: Boolean(dc) },
                //     hostForm[name].map(i =>
                //         m('option[selected]', { value: i.trim() }))
                // ),

            ]);
        },

        oncreate(vnode){


            vnode.dom.addEventListener('beforeinput', function(e){
                const input = vnode.dom.control;

                if(e.inputType == 'insertLineBreak' || delimiters.test(e.data)){
                    if(input.value.length < 1){
                        e.preventDefault();
                        return false;
                    }
                    addChip(input.value);
                    input.focus();
                    input.value = '';
                    e.preventDefault();
                    m.redraw();
                    return false;
                }

                if(values.size > 0 && input.value.length == 0 && e.inputType == 'deleteContentBackward'){
                    removeChip(data[name].slice(-1)[0]);
                    m.redraw();
                }
            });

            vnode.dom.addEventListener('focusout', function(){
                if(vnode.dom.control.value.length > 0){
                    addChip(vnode.dom.control.value);
                    vnode.dom.control.value = '';
                    m.redraw();
                }
            });
        }

    };
}

ChipsField.formControl = true;
