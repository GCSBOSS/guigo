import m from 'mithril';
import './radios.css';
import { clickRipple, getNameFromString } from './util';

// export function Radio (){

//     let name, data = {};

//     return {

//         oncreate(vnode){
//             vnode.dom.control.addEventListener('input', function(){
//                 data[name] = vnode.dom.control.value;
//                 m.redraw();
//             });
//         },

//         oninit(vnode){
//             name = vnode.attrs.name ||
//                 vnode.attrs.label.toLowerCase().replace(/[^a-z]+/g, '-');
//         },

//         view(vnode){
//             const type = vnode.attrs.type || 'text';
//             data = vnode.attrs.bind || {};

//             return m('label', [
//                 vnode.attrs.label,
//                 m('input', { type })
//             ])
//         }

//     };

// }

export function RadioGroup (){

    let name, data = {}, errors = {}, onchange;

    async function onclick(ev){
        console.log(ev.target.parentNode);
        await clickRipple(ev.target.parentNode, ev);
        onchange?.(this.value);
    }

    return {

        oncreate(vnode){
            vnode.dom.addEventListener('change', function(ev){
                delete errors[name];
                data[name] = ev.target.value;
                m.redraw();
            });
        },

        view(vnode){
            name = vnode.attrs.name || getNameFromString(vnode.attrs.label ?? '');
            onchange = vnode.attrs.onchange
            vnode.attrs.type = 'password';
            data = vnode.attrs.bind ?? {};
            errors = vnode.attrs.bindError ?? {};
            const opts = vnode.attrs.options ?? {};
            console.log(data[name]);
            return m('fieldset.radios', { 'data-error': errors[name] },
                m('label', vnode.attrs.label),
                Object.keys(opts).map(k =>
                    m('label.radio', m('input[type=radio]', {
                        value: k, name, onclick, checked: data[name] == k
                    }), opts[k])));
        }

    };

}

RadioGroup.formControl = true;