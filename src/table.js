import m from 'mithril';

/**
 * @typedef ColumnOpts
 * @property {boolean} hidden
 * @property {string} title
 * @property {string} field
 * @property {boolean} render
 * @property {Array<Record<string, unknown>>} data
 */

/**
 * @typedef TableAttrs
 * @property {boolean} loading
 * @property {Array<ColumnOpts>} columns
 * @property {boolean} selection
 * @property {Array<Record<string, unknown>>} data
 */

/**
 *
 * @returns {m.Component<TableAttrs>}
 */
export function Table(){

    return {



        view(vnode){

            let { loading, data, columns, selection } = vnode.attrs;

            loading = loading?.() ?? loading;



            return m('table', [

                m('thead', [

                ]),

                m('tbody', [

                ])

            ])



        }
    }
}

m(Table, {
    columns: [

        {

        }
    ]
})