import m from 'mithril';
import { delay/*, fadeIn, fadeOut, filterAttrs, parseClasses*/ } from './util';
import './list.css';
import './context-menu.css';
import { Panel } from './panel';

export const menuState = {};

export function ContextMenu(){

    return {

        view(vnode){

            const { context, content, event, ...rest } = vnode.attrs;

            return m(Panel, {
                as: 'menu',
                point: rest.ref ? undefined : event,
                vert: 'from top',
                ...rest,
                onblur: () => contextMenu(false),
                onclick: () => setTimeout(() => contextMenu(false), 100)
            }, content && m(content, context));
        }
        // fadeIn(vnode);
        // await fadeOut(vnode);
    };
}

/**
 * @param {Object} opts
 * @param {Event} opts.event Event that will trigger the menu to open
 * @param {Element} opts.ref Element to be used as positioning reference
 * @param {Mithril.Component} opts.content Mithril component to be rendered inside the menu
 */
export async function contextMenu(opts){
    opts.event?.preventDefault();
    menuState.contextMenu = false;
    m.redraw();
    await delay(100);
    if(opts){
        menuState.contextMenu = opts;
        m.redraw();
    }
}
