

export const standardEasing = 'cubic-bezier(0.4, 0.0, 0.2, 1)';
export const deceleratedEasing = 'cubic-bezier(0.0, 0.0, 0.2, 1)';

import m from 'mithril';

export const routeOverrides = {
    stack: [],
    id: 0
};

window.Guigo = {
    routeOverrides
}

export function pushRouteOverride(callback){
    const id = routeOverrides.id++;
    history.scrollRestoration = 'manual';
    history.pushState(id, '');
    history.scrollRestoration = 'auto';
    routeOverrides.stack.push({ id, callback }); 
    return id;
}

window.addEventListener('popstate', ev => {
    if(routeOverrides.stack.length == 0)
        return;
    
    const o = routeOverrides.stack.pop();
    o.callback(ev, o);
});

export function parseClasses(...specs){
    let r = '';
    for(const s of specs)
        if(Array.isArray(s))
            r += parseClasses(...s);
        else if(typeof s == 'string')
            r += '.' + s;
    return r;
}

export function delay(ms){
    return new Promise(done => setTimeout(done, ms));
}

export function normalize(input){
    return Array.isArray(input) ? input : [input];
}

export function getNameFromString(str){
    return str.toLowerCase().replace(/[^a-z]+/g, '-').replace(/^\-+|\-+$/g, '');
}

function getAttrFromKey(key){
    return key.replace(/[A-Z]/g, m => '-' + m.toLowerCase());
}

export function rippleAction(fn){
    return async function(ev){
        await clickRipple(this, ev);
        await fn.call(this, ev);
        m.redraw();
    }
}

export function clickRipple(dom, ev){

    const ripple = document.createElement('div');
    ripple.classList.add('guigo-ripple');
    const bound = Math.max(dom.clientWidth, dom.clientHeight) * 3.14;
    ripple.style.setProperty('--ripple-size', String(bound) + 'px');
    ripple.style.setProperty('--ripple-x', String(ev.offsetX - bound / 2) + 'px');
    ripple.style.setProperty('--ripple-y', String(ev.offsetY - bound / 2) + 'px');

    dom.appendChild(ripple);

    const p = animate(ripple, [
        { transform: 'scale(0.1)' },
        { transform: 'scale(1)' }
    ], { /*easing: deceleratedEasing,*/ duration: 300 });

    p.then(() => animate(ripple, [
        { transform: 'scale(1)' },
        { opacity: 0, transform: 'scale(1)' }
    ], { duration: 700 }))
        .then(() => ripple.remove());

    return new Promise(done => setTimeout(done, 200));
}

export function filterAttrs(source, ...forward){
    const r = {};

    source.id && (r.id = source.id);

    if(typeof source.dataset == 'object'){
        for(const k in source.dataset)
            r['data-' + getAttrFromKey(k)] = source.dataset[k];
        delete source.dataset;
    }

    for(const k of forward)
        if(k in source)
            r[getAttrFromKey(k)] = source[k];

    return r;
}

export function animate(dom, kfs, opts){
    const a = dom.animate(kfs, { easing: standardEasing, ...opts });
    return new Promise(done => a.onfinish = done);
}

export function isNullish(value){
    return typeof value == 'undefined' || value === null;
}

export function fadeIn(vnode){
    animate(vnode.dom, [ { transform: 'scale(0.8)' }, { } ],
        { duration: 150, easing: 'cubic-bezier(0.0, 0.0, 0.2, 1)' })
    animate(vnode.dom, [ { opacity: 0 }, { opacity: 1 } ],
        { duration: 45, easing: 'linear' });
    animate(vnode.dom, [ { '--scrim-color': '#0000' }, {  } ],
        { duration: 150, easing: 'linear' });
}


export async function fadeOut(vnode){
    await animate(vnode.dom, [
        { opacity: 1 },
        { opacity: 0 }
    ], { duration: 75, easing: 'linear' });
}
