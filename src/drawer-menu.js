import './drawer-menu.css';
import m from 'mithril';
import { pushRouteOverride } from './util';

let menuOpen;
let poppedHistory = false;

export function drawerMenu(state){
    const oldState = menuOpen;
    menuOpen = state ?? !menuOpen;

    if(state === false && oldState != menuOpen){
        if(poppedHistory)
            poppedHistory = false;
        else{
            history.back();
        }
    }
    m.redraw();
}

export function DrawerMenu (){

    let leftOnHold, left, dragging = false, leftMin, pointZero, bgOpacity = 0, holdingStill;


    function onclick(ev){
        const { right } = ev.target.getBoundingClientRect();

        if(ev.clientX > right)
            drawerMenu(false);
    }

    function handlePopStateEvent(){
        poppedHistory = true;
        drawerMenu(false);
    }

    return {

        oninit(){
            leftMin = -(document.body.clientWidth - 56);
            left = leftMin;
        },

        view(vnode){

            leftMin = -(document.body.clientWidth - 56);

            if(!dragging)
                left = menuOpen ? 0 : leftMin;

            bgOpacity = (1 - left / leftMin) * 0.8;

            const classes = (menuOpen ? '' : '.closed') +
                (dragging ? '.dragging' : '');

            // for(let i of vnode.attrs.items)
            //     if(i.text && i.text == globalState.pageTitle){
            //         i.classes = '.active';
            //         break;
            //     }


            const attrs = {};

            if(vnode.attrs.icon)
                attrs['data-icon-pre'] = vnode.attrs.icon;

            return m('article.drawer-menu' + classes, {
                style: { '--bg-opacity': bgOpacity, left: left + 'px' },
                onclick,
                'data-open': menuOpen
            },
            [
                // m(Button, { icon: 'arrow_back', action: () => menuOpen = false }),
                vnode.children
                // vnode.attrs.header || [],
                // m('ul', vnode.attrs.items.map(i =>
                //     m('li', { onclick: i.action ?? Function.prototype },
                //         typeof i == 'string' ? i : i.text)))
            ]);
        },

        oncreate(vnode){
            pushRouteOverride(handlePopStateEvent);

            vnode.dom.addEventListener('touchstart', function(ev){
                pointZero = ev.touches[0].clientX;
                holdingStill = true;
                leftOnHold = menuOpen ? 0 : leftMin;
                setTimeout(function(){
                    if(holdingStill && left < 0){
                        left = leftMin + 16;
                        dragging = true;
                        m.redraw();
                    }
                }, 500);
                m.redraw();
            }, { passive: true });

            vnode.dom.addEventListener('touchmove', function(ev){
                dragging = true;
                holdingStill = false;

                const offset = pointZero - ev.touches[0].clientX;
                left = leftOnHold - offset;
                left = Math.max(Math.min(left, 0), leftMin);

                menuOpen = left >= leftMin * 0.5;

                m.redraw();
            }, { passive: true });

            vnode.dom.addEventListener('touchend', function(){
                holdingStill = false;
                dragging = false;
                m.redraw();
            }, { passive: true });
        }

    };

}