import m from 'mithril'
import { Session, State, clearSession } from './state';

import './dev-panel.css';
import { Button } from './button';

// const getCircularReplacer = () => {
//     const seen = new WeakSet();
//     return (key, value) => {
//         if(typeof value === 'object' && value !== null) {
//             if(seen.has(value))
//                 return;
//             seen.add(value);
//         }
//         return value;
//     };
// };

function decycle(object) {
    var objects = new WeakMap();

    return (function derez(value, path) {

        var oldPath;
        var nu;

        if(
            typeof value === 'object'
            && value !== null
            && !(value instanceof Boolean)
            && !(value instanceof Date)
            && !(value instanceof Number)
            && !(value instanceof RegExp)
            && !(value instanceof String)
        ) {

            oldPath = objects.get(value);
            if(oldPath !== undefined)
                return {$ref: oldPath};

            objects.set(value, path);

            if(Array.isArray(value)) {
                nu = [];
                value.forEach(function (element, i) {
                    nu[i] = derez(element, path + '[' + i + ']');
                });
            }
            else{

                nu = {};
                Object.keys(value).forEach(function (name) {
                    nu[name] = derez(
                        value[name],
                        path + '[' + JSON.stringify(name) + ']'
                    );
                });
            }
            return nu;
        }
        return value;
    }(object, '$'));
};

export const devPanelState = {
    open: false,
    inspects: {
        State, 
        Session
    }
};

export function dev(inspects){
    if(inspects)
        Object.assign(devPanelState.inspects, inspects);
    else
        devPanelState.open = !devPanelState.open;
    m.redraw();
};

window.dev = dev;

function RenderNode(){

    const toggles = {};

    return {

        view(vnode){
            const node = vnode.attrs.node;
            
            if(node === null)
                return m('span.null', 'null');

            if(typeof node == 'object'){

                return [
                    m('span.object', [
                        Object.keys(node).map(k => {
                            return m('span.line', [
                                typeof node[k] == 'object' && node[k] !== null &&
                                    m('span.toggle', {
                                        'data-icon': (k in toggles ? toggles[k] : vnode.attrs.depth != 4) ? 'remove' : 'add',
                                        onclick: () => toggles[k] = !toggles[k]
                                    }),
                                m('span.key', k + ':'),
                                
                                typeof node[k] == 'object' && node[k] !== null 
                                    ? [
                                        m('span.open', Array.isArray(node[k]) ? '[' : '{'),

                                        (k in toggles ? toggles[k] : vnode.attrs.depth != 4 ) 
                                            ? m(RenderNode, { node: node[k], depth: (vnode.attrs.depth ?? 0) + 1 })
                                            : m('span.count', Object.values(node[k]).length + ' items'),
                                        
                                        m('span.close', Array.isArray(node[k]) ? ']' : '}'),
                                    ]

                                    : m(RenderNode, { node: node[k] })
                            ])
                        }),
                    ]),
                ] 
            }

            return m('span.' + typeof node, JSON.stringify(node));
        }
    }

}

function renderJSONViewer(obj){
    const data = decycle(obj);
    return m(RenderNode, { node: data });
}

export function DevPanel(){

    let position = 'fullscreen';

    return {

        view(){

            return m('aside#dev-panel', { 'data-position': position }, [
                m('header', [
                    m(Button, { disabled: position == 'fullscreen', icon: 'fullscreen', action: () => position ='fullscreen' }),
                    m(Button, { disabled: position == 'dock', icon: 'dock_to_bottom', action: () => position = 'dock' }),
                    m(Button, { icon: 'close', action: () => devPanelState.open = false }),
                ]),

                Object.keys(devPanelState.inspects).map(name => {

                    return [
                        m('label', name),
                        renderJSONViewer(devPanelState.inspects[name])
                    ]
                })

                // m('pre', JSON.stringify(decycle(State), null, 4)),
                // m('br'),

                // m('label', 'Session'),
                // m('pre', JSON.stringify(Session, null, 4)),
                // m('button', { onclick: clearSession }, 'Clear Session')
            ]);
        }
    }
}