
import m from 'mithril';

type ElementAttrs = {
    /** HTML element id */
    id?: string 
    /** String in the form `.class1.class2` */
    class?: string 
    /** Hint to be displayed on hover */
    title?: string 
    disabled?: boolean 
    hidden?: boolean
    tabindex?: number
}

type FormControlAttrs = {
    /** name of the control in the form data */
    name?: string 
    /** Form data object */
    bind?: Record<string, unknown>
    /** Form error object */
    bindError?: Record<string, string>
    /** Label text to be displayed above the control */
    label?: string
    /** A fixed value to be passed to the control */
    value?: unknown
}

type ButtonAttrs = ElementAttrs & {
    /** Whether events should `stopPropagation` */
    noBubble?: boolean 
    /** Text to be displayed in the button  */
    text?: string 
    /** onclick event listener */
    action?: (event: PointerEvent) => void 
    /** Icon to be displayed */
    icon?: string
    type?: 'button' | 'submit'
}

type TextFieldAttrs = ElementAttrs & FormControlAttrs & {
    /** Icon to be displayed */
    icon?: string
    maxlength?: number
    placeholder?: string
    onkeyup?: (event: KeyboardEvent) => void 
    oninput?: (event: KeyboardEvent) => void 
    autocomplete?: AutoFill
    onclick?: (event: PointerEvent) => void 
    /** 
     * Whether pressing ENTER while focusing the field 
     * will submit it's form 
     */
    submit?: boolean
    /** Whether to wrap input text lines */
    wrap?: boolean
    /** Whether to render a date picker field */
    dateTime?: boolean
    /** Whether to display redacted user input */
    secret?: boolean
    /** 
     * Time in milliseconds to wait after user finish 
     * typing before triggering oninput event 
     */
    debounce?: number
}

export const Button: () => m.Component<ButtonAttrs>
export const TextField: () => m.Component<TextFieldAttrs>
// export const RadioGroup: () => m.Component<RadioGroupAttrs>
// export const Form: () => m.Component<FormAttrs>
// export const DrawerMenu: () => m.Component<DrawerMenuAttrs>
// export const Panel: () => m.Component<PanelAttrs>
// export const Chip: () => m.Component<ChipAttrs>
// export const ChipsField: () => m.Component<ChipsFieldAttrs>
// export const ChipList: () => m.Component<ChipListAttrs>
// export const Select: () => m.Component<SelectAttrs>
// export const CheckBox: () => m.Component<CheckBoxAttrs>
// export const ChipList: () => m.Component<ChipListAttrs>





type SnackOpts = {
    /** delay before triggering snack in seconds */
    delay?: number
}

/** Triggers a snack with a message */
export function snackBar(text: string, opts: SnackOpts): void




// export { Session, State, clearSession } from './state';
// export { contextMenu } from './context-menu';
// export { dialog } from './dialog';
// export { animate, rippleAction } from './util';
// export async function guigo(opts)
// export function title(name)
// export function comp(vnode)
// export function view(id, ...children)