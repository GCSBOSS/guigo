let localChanged = false;
const loadedLocal = JSON.parse(localStorage.getItem('_state') || '{}');

export const State = {};

export const Session = new Proxy(loadedLocal, {

    set(obj, key, value){
        localChanged = true;
        obj[key] = value;
        return true;
    },

    get(obj, key){
        localChanged = true;
        return obj[key];
    }

});

export function clearSession(){
    for(const key in loadedLocal)
        delete loadedLocal[key];
    localStorage.setItem('_state', '{}');
}

window.addEventListener('storage', (ev) => {
    const externalVal = JSON.parse(ev.newValue);
    if(localChanged){
        localChanged = false;
        Object.assign(externalVal, loadedLocal);
        localStorage.setItem('_state', JSON.stringify(externalVal));
    }
    else
        Object.assign(loadedLocal, externalVal);
});

setInterval(function(){
    if(localChanged)
        localStorage.setItem('_state', JSON.stringify(loadedLocal));
    localChanged = false;
}, 1000);

window.addEventListener('beforeunload', function(){
    if(localChanged)
        localStorage.setItem('_state', JSON.stringify(loadedLocal));
});


window.State = State;
window.Session = Session;