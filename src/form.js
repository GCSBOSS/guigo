import m from 'mithril';

export function Form (){

    return {

        view(vnode){

            function bindRecursive(vn){
                if(!Array.isArray(vn.children))
                    return;

                for(const c of vn.children.flat()){
                    if(c?.tag?.formControl){
                        c.attrs.bind = vnode.attrs.data;
                        c.attrs.bindError = vnode.attrs.errors;
                    }
                    else if(Array.isArray(c.children))
                        bindRecursive(c);

                    if(c?.attrs?.submit)
                        c.attrs.disabled = Object.values(vnode.attrs.errors).length > 0;
                }
            }

            bindRecursive(vnode);

            return m('form', {
                id: vnode.attrs.id,
                onsubmit: e => e.preventDefault()
            }, vnode.children);
        }

    };

}