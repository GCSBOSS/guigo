
import m from 'mithril';

import './free-box.css';
import { pushRouteOverride } from './util';

export function FreeBox(){

    let bind, name, value, attrs, roid;


    function onHistoryBack(){
        if(bind[name] && attrs.routing)
            bind[name] = false;
    }

    function onShow(){
        console.log('THIS IS RUNNING');
        if(attrs.routing)
            roid = pushRouteOverride(onHistoryBack);
    }

    function onHide(){
        console.log('THIS IS HHH');

        if(attrs.routing && history.state == roid)
            history.back();
    }

    function onScrimClick(){
        bind[name] = false;
    }

    return {

        view(vnode){

            const { as, bind: _bind, name: _name, uniqueKey, position, routing, modal, class: cls, ...rest } = vnode.attrs;

            attrs = vnode.attrs;
            bind = _bind;
            name = _name
            

            const oldValue = value;

            value = bind && name ? bind[name] : true;

            // console.log(JSON.stringify(oldValue), JSON.stringify(value), oldValue != value)
            if(bind && name && Boolean(oldValue) != Boolean(value))
                bind[name] ? onShow() : onHide();

            if(!value)
                return null;

            return [
                modal && m('div.guigo-scrim', {
                    onclick: onScrimClick
                }),
                m((as ?? 'div') + '.guigo-free' + (cls ?? ''), rest, vnode.children),
            ];

        }
    }

}
