import m from 'mithril';
import { contextMenu } from './context-menu';
import './select.css';
import { clickRipple, getNameFromString } from './util';


export function Select(){

    let name, data = {}, errors = {}, items, onselect, textPrp;

    function index(items){

        // if(Array.isArray(items)){
        //     const newItems = {};

        //     for(const i of items){



        //     }
        // }
        // TODO support array of values
        // TODO support array of objects (text, value)
        // TODO support array of objects (custom fields)
        return items;
    }

    function text(v){
        let finalText = items[v];

        if(typeof finalText == 'object')
            if(textPrp)
                finalText = finalText[textPrp];
            else
                finalText = JSON.stringify(finalText);

        return finalText;
    }

    async function onInputClick(ev){
        await clickRipple(ev.target.parentNode, ev);
        contextMenu({
            // event: ev,
            as: 'ul.dropdown',
            vert: 'bottom',
            horiz: 'cover',
            ref: this,
            content: () => ({ view: () => Object.entries(items).map(i =>
                m('li', {
                    onclick: () => {
                        data[name] = i[0];
                        onselect?.(i[0], i[1]);
                    }
                }, text(i[0])))
            })
        })
    }

    return {

        view(vnode){

            const { value, name: nm, label, bind, bindError, icon,
                items: is, textProp, ...rest } = vnode.attrs;

            textPrp = textProp;
            items = index(is ?? {});
            name = nm ?? getNameFromString(label ?? '');
            data = bind ?? {};
            errors = bindError ?? {};
            onselect = rest.onselect;

            if(value)
                data[name] = value;

            return m('label.text-field.select', {
                'data-error': errors[name],
                'data-icon-pos': 'arrow_drop_down',
                onclick: onInputClick
            }, [
                m('span', label),
                icon && m('span', { 'data-icon': icon }),
                m('input[type=text]', {
                    placeholder: rest.placeholder,
                    readonly: true, value: text(data[name]) })
            ]);
        }
    }
}

Select.formControl = true;
