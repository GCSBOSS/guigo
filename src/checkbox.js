import m from 'mithril';
import './checkbox.css';
import { filterAttrs, getNameFromString } from './util';



export function CheckBox(){

    let name, data = {}, errors = {};

    return {

        oncreate(vnode){

            vnode.dom.control.addEventListener('change', function(e){
                delete errors[name];
                data[name] = vnode.dom.control.checked;

                vnode.attrs.onchange?.(e);

                m.redraw();
            });
        },

        view(vnode){
            name = vnode.attrs.name || getNameFromString(vnode.attrs.label || '');
            data = vnode.attrs.bind || {};
            errors = vnode.attrs.bindError || {};

            const attrs = filterAttrs(vnode.attrs, 'disabled');

            return m('label.checkbox', { 'data-error': errors[name] }, [
                m('input[type=checkbox]', { ...attrs, name, checked: vnode.attrs.checked ?? data[name] }),
                m('span', vnode.attrs.label)
            ]);
        }
    }
}

CheckBox.formControl = true;
