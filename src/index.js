import './colors.css';
import './index.css';

import m from 'mithril';
export { TextField } from './text-field';
export { Button } from './button';
export { snackBar } from './snack-bar';
export { RadioGroup } from './radios';
export { Form } from './form';
export { Session, State, clearSession } from './state';
export { contextMenu } from './context-menu';
export { DrawerMenu, drawerMenu } from './drawer-menu';
export { Panel } from './panel';
export { dialog } from './dialog';
export { DevPanel } from './dev-panel';
export { Chip, ChipsField, ChipList } from './chips';
export { Select } from './select';
export { CheckBox } from './checkbox';
export { animate, rippleAction } from './util';

import { menuState, ContextMenu } from './context-menu';
import { snackState, SnackBar } from './snack-bar';
// import { DevPanel } from './dev-panel';
import { State } from './state';
import { reportError } from './diagnostics';
import { DevPanel, devPanelState } from './dev-panel';
import { dialogState, Dialog } from './dialog';

export async function guigo(opts){
    opts = opts || {};
    const views = opts.views || {};

    // if(opts.title)
    // globalState.appTitle = opts.title;

    function handleError(ev){
        const err = ev?.error ?? ev?.reason;
        console.log('got bef')
        // console.error(err);
        // snackBar(String(err), { class: 'error' });
        opts.diagnostic && reportError(ev, err, opts.diagnostic);
    }

    m.route.prefix = '';

    const pathGuards = {};
    let fallBackGuard;
    if(Array.isArray(opts.guards))
        for(const g of opts.guards)
            if(!g.routes)
                fallBackGuard = g;
            else for(const p of g.routes)
                pathGuards[p] = g;

    const routes = {};
    for(const path in views){

        routes[path] = {
            onmatch: function(args, requestedPath, route) {
                
                if(pathGuards[path])
                    pathGuards[path].fn();
                else if(fallBackGuard)
                    fallBackGuard.fn();
                
                const view = views[path];
                title(view?.()?.title);
                return view;
            }
        }
    }

    if(opts.notFound)
        routes['/:notFound...'] = opts.notFound;

    await new Promise(done => window.addEventListener('load', done));
    //await new Promise(done => document.onreadystatechange = done);

    // TODO wait for fonts to fully load

    document.body.classList.add('app');

    const appHeight = () => document.documentElement.style.setProperty('--app-height',
        window.innerHeight + 'px');
    window.addEventListener('resize', appHeight);
    appHeight();

    m.route(document.body, opts.home || '/', routes);

    if(opts.worker)
        State.worker = await navigator.serviceWorker.register(opts.worker);

    window.addEventListener('error', ev => {
        ev.preventDefault()
        handleError(ev)
    });
    
    window.addEventListener('unhandledrejection', ev => {
        ev.preventDefault()
        handleError(ev)
    });

    if(opts.favicon){
        const lnk = document.createElement('link');
        lnk.rel = 'icon';
        lnk.href = opts.favicon;
        document.head.appendChild(lnk);
    }
}

const backStack = [];

m.route.rawSet = m.route.set;
m.route.set = (function(...args){
    backStack.push([ m.route.get(), m.route.param() ]);
    m.route.rawSet(...args);
}).bind(m.route);

m.route.back = function(path, data = {}){
    const prev = backStack.pop();

    const prevUrl = m.buildPathname(prev?.[0], prev?.[1])?.split('?')[0];
    const targetUrl = m.buildPathname(path, data)?.split('?')[0];
    const curRoute = [ m.route.get(), m.route.param() ];

    if(prevUrl != targetUrl){
        // TODO might need to set a proper title for this first state that will be popped
        history.replaceState(data, '', path);
        history.pushState(curRoute[1], '', curRoute[0]);
    }
    history.back();
}

export function title(name){
    const appName = (document.title.split(' - ')[1] ?? document.title);
    if(name)
        document.title = name + ' - ' + appName;
    else 
        document.title = appName;
}

export function comp(vnode){
    return function (){
        return {
            view(){
                return vnode;
            }
        }
    }
}

export function view(id, ...children){
    return [
        // vnode.attrs.appBar && m(AppBar, vnode.attrs.appBar),
        m('main#' + id, ...children),
        // globalState.navDrawerComponent && m(globalState.navDrawerComponent),
        m('aside#bottom-stack', [
            snackState.snackBar && m(SnackBar, snackState.snackBar),
            // globalState.fab && m(FAB, globalState.fab),
        ]),
        menuState.contextMenu && m(ContextMenu, menuState.contextMenu),
        m(Dialog, dialogState.children),
        devPanelState.open && m(DevPanel)
        // globalState.blockingLoader && m(BlockingLoader),
    ];
}

// Object.assign(m, { view, guigo/*, worker: () => globalState.worker*/ });

// export default m;
