import m from 'mithril';
import { fadeIn, fadeOut, pushRouteOverride } from './util';
import './panel.css';

export function Panel(){

    let vert, horiz, onblur, ref, ttp, dock, point;

    function setPosition(){

        let left = 0, top = 0, bottom = 0, right = 0;

        if(dock == 'bottom'){
            top = null;
            bottom = 0;

            ttp.style.width = '100vw';
            horiz = 'center';
            vert = 'bottom';
        }

        else if(ref){
            const ttpRect = ttp.getBoundingClientRect();
            const refRect = ref.getBoundingClientRect();

            bottom = null;
            right = null;

            left = refRect.left;
            top = refRect.bottom;

            if(vert == 'top')
                top = refRect.top - ttpRect.height - 8;

            if(vert == 'bottom')
                top = refRect.bottom;

            if(vert == 'from top')
                top = refRect.top;

            if(horiz == 'cover')
                ttp.style.width = refRect.width + 'px';

            if(horiz == 'center')
                left -= ttpRect.width / 2 - refRect.width / 2;

            if(horiz == 'from right')
                left -= ttpRect.width - refRect.width;

            left = Math.max(left, 8);
            top = Math.max(top, 8);
            if(left + ttpRect.width > window.innerWidth - 8)
                left = window.innerWidth - 8 - ttpRect.width;
            if(top + ttpRect.height > window.innerHeight - 8)
                top = window.innerHeight - 8 - ttpRect.height;
        }

        ttp.style.transformOrigin = vert.replace('from ', '') + ' ' + horiz.replace('from ', '');
        ttp.style.top = String(top) + 'px';
        ttp.style.left = String(left) + 'px';
        ttp.style.right = String(right) + 'px';
        ttp.style.bottom = String(bottom) + 'px';
    }

    let blurred = false;
    let poppedHistory = false;

    function onClickModal(){
        runBlurListener();
        popHistory();
    }

    function popHistory(){
        if(poppedHistory)
            return;

        poppedHistory = true;
        history.back();
    }

    function runBlurListener(){
        if(blurred)
            return;

        blurred = true;
        onblur?.();
    }

    return {

        view(vnode){
            vert = vnode.attrs.vert ?? 'top';
            horiz = vnode.attrs.horiz ?? 'left';
            onblur = vnode.attrs.onblur;
            dock = vnode.attrs.dock;
            ref = vnode.attrs.ref;
            point = vnode.attrs.point;
            const tag = vnode.attrs.as ?? 'aside';


            if(point)
                ref = { getBoundingClientRect: () => ({
                    top: point.clientY,
                    left: point.clientX,
                    right: point.clientX,
                    bottom: point.clientY,
                    height: point.target.height,
                    width: point.target.width
                }) };

            return [
                m(tag + '.panel', {
                    onclick: vnode.attrs.onclick,
                    class: vnode.attrs.class,
                    id: vnode.attrs.id
                }, vnode.children),
                m('div.panel-modal', { onclick: onClickModal })
            ];
        },

        oncreate(vnode){
            pushRouteOverride(() => {
                poppedHistory = true;
                runBlurListener();
            });

            ttp = vnode.dom;
            setPosition();
            const curTop = document.body.scrollTop;
            document.body.style.overflow = 'hidden';
            document.body.style.marginTop = '-' + curTop + 'px';
            fadeIn(vnode);
        },

        onremove(){
            popHistory();
            document.body.style.overflow = null;
            document.body.style.marginTop = null;
        },

        async onbeforeremove(vnode){
            await fadeOut(vnode);
        },

        onupdate(vnode){
            ttp = vnode.dom;
            setPosition();
        }

    };
}
